﻿using System;
using System.Collections.Generic;
using System.Text;

namespace voltmeter
{
    class Voltage : Voltmeter
    {
        public Voltage()
        {
            unit = Constants.STR_UNIT_VOLTAGE;
            type = Constants.STR_VOLTAGE;
        }
    }
}
