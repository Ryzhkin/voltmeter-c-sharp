﻿using System;
using System.Collections.Generic;
using System.Text;

namespace voltmeter
{
    public class Constants
    {
        public static readonly int TYPE_VOLTAGE = 1;
        public static readonly int TYPE_AMPERAGE = 2;
        public static readonly int TYPE_RESISTANCE = 3;

        public static readonly int MIN_SCALE = 5;
        public static readonly int MAX_SCALE = 50;
        public static readonly int MIN_SCALE_RESISTANCE = 5;
        public static readonly int MAX_SCALE_RESISTANCE = 1000;
        public static readonly int MIN_DIMENSION = 0;
        public static readonly int MIN_SCALE_RESULT = 0;

        public static readonly double DIMENSION_RESISTANCE_1 = 0.01;
        public static readonly double DIMENSION_RESISTANCE_2 = 0.1;
        public static readonly double DIMENSION_RESISTANCE_3 = 1.0;
        public static readonly double DIMENSION_RESISTANCE_4 = 10.0;

        public static readonly string STR_VOLTAGE = "Напряжение";
        public static readonly string STR_AMPERAGE = "Ток";
        public static readonly string STR_RESISTANCE = "Сопротивление";
        public static readonly string STR_UNIT_VOLTAGE = "(V)";
        public static readonly string STR_UNIT_AMPERAGE = "(A)";
        public static readonly string STR_UNIT_RESISTANCE = "(kOm)";

        public static readonly string STR_INPUT_TYPE = "Введите № величины: ";
        public static readonly string STR_INPUT_TYPE_ERROR = "Неверный № величины!";
        public static readonly string STR_INPUT_SCALE = "Введите диапазон шкалы (V, A)";
        public static readonly string STR_INPUT_SCALE_RESISTANCE = "Введите диапазон шкалы (kOm)";
        public static readonly string STR_INPUT_SCALE_ERROR = "Неверный диапазон шкалы!";
        public static readonly string STR_INPUT_SCALE_RESISTANCE_ERROR = "Неверный диапазон шкалы!";
        public static readonly string STR_INPUT_DIMENSION = "Введите выставленное значение на приборе: ";
        public static readonly string STR_INPUT_DIMENSION_ERROR = "Неверное выставленное значение на приборе! Значение не может быть отрицательным!";
        public static readonly string STR_INPUT_DIMENSION_RESISTANCE = "Введите выставленное значение (множитель x) на приборе (0.01, 0.1, 1, 10): ";
        public static readonly string STR_INPUT_DIMENSION_RESISTANCE_ERROR = "Неверное выставленное значение на приборе!";
        public static readonly string STR_INPUT_SCALE_RESULT = "Введите результат по шкале: ";
        public static readonly string STR_INPUT_SCALE_RESULT_ERROR = "Неверный результат по шкале!";
    }
}
