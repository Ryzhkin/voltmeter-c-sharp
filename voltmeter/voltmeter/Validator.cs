﻿/**
 * Class Validator необходим для валидации входных данных (проверка корректности входных данных).
 * При ложной валидации данных, пользователь вводит входные данных до тех пор, когда данные пройдут валидацию по заданным условиям.
 * Если валидация успешна, то вызванный метод возращает значение.
 */

using System;
using System.Collections.Generic;
using System.Text;

namespace voltmeter
{
    class Validator
    {
        // значения по умолчанию.
        private int valueInt = 0;
        private double valueDouble = 0;

        /**
         * Конструктор с 1 целочисленным аргументом.
         * @param value переданное значение в конструктор
         */
        public Validator(int valueInt) => this.valueInt = valueInt;

        /**
         * Конструктор с 1 вещественным аргументом.
         * @param value переданное значение в конструктор
         */
        public Validator(double valueDouble) => this.valueDouble = valueDouble;

        /**
         * Проверка № величины.
         * @return № величины, которая успешно прошла валидацию данных.
         */
        public int Type()
        {
            while (valueInt != Constants.TYPE_VOLTAGE && valueInt != Constants.TYPE_AMPERAGE && valueInt != Constants.TYPE_RESISTANCE)
            {
                Console.WriteLine($"{Constants.STR_INPUT_TYPE_ERROR}");
                Console.WriteLine($"{Constants.STR_INPUT_TYPE}");
                valueInt = Convert.ToInt32(Console.ReadLine());
            }

            return valueInt;
        }

        /**
         * Проверка шкалы.
         * @return значение шкалы, которая успешно прошла валидацию данных.
         */
        public int Scale()
        {
            while (valueInt < Constants.MIN_SCALE || valueInt > Constants.MAX_SCALE)
            {
                Console.WriteLine(Constants.STR_INPUT_SCALE_ERROR);
                Console.WriteLine($"{Constants.STR_INPUT_SCALE} от {Constants.MIN_SCALE} до {Constants.MAX_SCALE}: ");
                valueInt = Convert.ToInt32(Console.ReadLine());
            }

            return valueInt;
        }

        /**
         * Проверка шкалы для сопротивления.
         * @return значение шкалы для сопротивления, которая успешно прошла валидацию данных.
         */
        public int ScaleResistance()
        {
            while (valueInt < Constants.MIN_SCALE_RESISTANCE || valueInt > Constants.MAX_SCALE_RESISTANCE)
            {
                Console.WriteLine(Constants.STR_INPUT_SCALE_RESISTANCE_ERROR);
                Console.WriteLine($"{Constants.STR_INPUT_SCALE_RESISTANCE} от {Constants.MIN_SCALE_RESISTANCE} до {Constants.MAX_SCALE_RESISTANCE}: ");
                valueInt = Convert.ToInt32(Console.ReadLine());
            }

            return valueInt;
        }

        /**
         * Проверка выставленной величины на приборе.
         * @return значение величины, которая успешно прошла валидацию данных.
         */
        public double Dimension()
        {
            while (valueDouble < Constants.MIN_DIMENSION)
            {
                Console.WriteLine(Constants.STR_INPUT_DIMENSION_ERROR);
                Console.Write(Constants.STR_INPUT_DIMENSION);
                this.valueDouble = Convert.ToDouble(Console.ReadLine());
            }

            return valueDouble;
        }

        /**
         * Проверка выставленной величины на приборе для сопротивления.
         * @return значение величины для сопротивления, которая успешно прошла валидацию данных.
         */
        public double DimensionResistance()
        {
            while (valueDouble != Constants.DIMENSION_RESISTANCE_1 && valueDouble != Constants.DIMENSION_RESISTANCE_2 &&
                    valueDouble != Constants.DIMENSION_RESISTANCE_3 && valueDouble != Constants.DIMENSION_RESISTANCE_4)
            {
                Console.WriteLine(Constants.STR_INPUT_DIMENSION_RESISTANCE_ERROR);
                Console.Write(Constants.STR_INPUT_DIMENSION_RESISTANCE);
                valueDouble = Convert.ToDouble(Console.ReadLine());
            }

            return valueDouble;
        }

        /**
         * Проверка результата по шкале.
         * @return значение шкалы, которая успешно прошла валидацию данных.
         */
        public double ScaleResult()
        {
            while (valueDouble < Constants.MIN_SCALE_RESULT && valueDouble > Constants.MAX_SCALE)
            {
                Console.WriteLine(Constants.STR_INPUT_SCALE_RESULT_ERROR);
                Console.Write(Constants.STR_INPUT_SCALE_RESULT);
                valueDouble = Convert.ToDouble(Console.ReadLine());
            }

            return valueDouble;
        }

        /**
         * Проверка результата по шкале для сопротивления.
         * @return значение шкалы для сопротивления, которая успешно прошла валидацию данных.
         */
        public double ScaleResultResistance()
        {
            while (valueDouble < Constants.MIN_SCALE_RESULT && valueDouble > Constants.MAX_SCALE_RESISTANCE)
            {
                Console.WriteLine(Constants.STR_INPUT_DIMENSION_RESISTANCE_ERROR);
                Console.Write(Constants.STR_INPUT_DIMENSION_RESISTANCE);
                valueDouble = Convert.ToDouble(Console.ReadLine());
            }

            return valueDouble;
        }
    }
}