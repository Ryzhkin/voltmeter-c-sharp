﻿using System;
using System.Collections.Generic;
using System.Text;

namespace voltmeter
{
    class Resistance : Voltmeter
    {
        public Resistance()
        {
            unit = Constants.STR_UNIT_RESISTANCE;
            type = Constants.STR_RESISTANCE;
        }

        public new void Input()
        {
            InputScale();
            InputDimension();
            InputScaleResult();
        }

        public new void InputScale()
        {
            Console.Write($"{Constants.STR_INPUT_SCALE_RESISTANCE} от {Constants.MIN_SCALE_RESISTANCE} до {Constants.MAX_SCALE_RESISTANCE}: ");
            scale = Convert.ToInt32(Console.ReadLine());

            Validator validator = new Validator(this.scale);
            scale = validator.ScaleResistance();
        }

        public new void InputDimension()
        {
            Console.Write(Constants.STR_INPUT_DIMENSION_RESISTANCE);
            dimension = Convert.ToDouble(Console.ReadLine());

            Validator validator = new Validator(dimension);
            dimension = validator.DimensionResistance();
        }

        public new void InputScaleResult()
        {
            Console.Write(Constants.STR_INPUT_SCALE_RESULT);
            scaleResult = Convert.ToDouble(Console.ReadLine());

            Validator validator = new Validator(scaleResult);
            scaleResult = validator.ScaleResultResistance();
        }
        
        public new void Calculate()
        {
            if (dimension == Constants.DIMENSION_RESISTANCE_1) result = scaleResult / 100;
            if (dimension == Constants.DIMENSION_RESISTANCE_2) result = scaleResult / 10;
            if (dimension == Constants.DIMENSION_RESISTANCE_3) result = scaleResult;
            if (dimension == Constants.DIMENSION_RESISTANCE_4) result = scaleResult * 100;
        }
    }
}
