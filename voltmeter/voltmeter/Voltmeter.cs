﻿using System;
using System.Collections.Generic;
using System.Text;

namespace voltmeter
{
    abstract class Voltmeter
    {
        protected double dimension;
        protected double scaleResult;
        protected double result;
        protected int scale;
        protected string unit;
        protected string type;

        public Voltmeter()
        {
            dimension = 0;
            scaleResult = 0;
            result = 0;
            scale = 0;
            unit = "";
            type = "";
        }

        public void Input()
        {
            InputScale();
            InputDimension();
            InputScaleResult();
        }

        public void InputScale()
        {
            Console.WriteLine($"{Constants.STR_INPUT_SCALE} от {Constants.MIN_SCALE} до {Constants.MAX_SCALE}: ");
            scale = Convert.ToInt32(Console.ReadLine());

            Validator validatorScale = new Validator(scale);
            scale = validatorScale.Scale();
        }

        public void InputDimension()
        {
            Console.Write(Constants.STR_INPUT_DIMENSION);
            dimension = Convert.ToDouble(Console.ReadLine());

            Validator validatorDimension = new Validator(dimension);
            dimension = validatorDimension.Dimension();
        }

        public void InputScaleResult()
        {
            Console.Write(Constants.STR_INPUT_SCALE_RESULT);
            scaleResult = Convert.ToDouble(Console.ReadLine());

            Validator validatorScaleResult = new Validator(scaleResult);
            scaleResult = validatorScaleResult.ScaleResult();
        }

        public void Calculate()
        {
            result = dimension / scale * scaleResult;
        }

        public void Output()
        {
            Console.WriteLine($"{type} = {result} {unit}");
        }
    }
}
