﻿using System;
using System.Collections.Generic;
using System.Text;

namespace voltmeter
{
    class Amperage : Voltmeter
    {
        public Amperage()
        {
            unit = Constants.STR_UNIT_AMPERAGE;
            type = Constants.STR_AMPERAGE;
        }
    }
}
