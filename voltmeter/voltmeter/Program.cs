﻿using System;

namespace voltmeter
{
    class Program
    {
        static void Main(string[] args)
        {
            Type type = new Type();
            type.Input();

            // Если введена величина "Напряжение".
            if (type.GetType() == Constants.TYPE_VOLTAGE)
            {
                Voltage voltage = new Voltage();
                // Ввод данных в консоле
                voltage.Input();
                // Расчет по формуле
                voltage.Calculate();
                // Вывод данных в консоле
                voltage.Output();
            }

            // Если введена величина "Ток"
            if (type.GetType() == Constants.TYPE_AMPERAGE)
            {
                Amperage amperage = new Amperage();
                // Ввод данных в консоле
                amperage.Input();
                // Расчет по формуле
                amperage.Calculate();
                // Вывод данных в консоле
                amperage.Output();
            }

            // Если введена величина "Сопротивление"
            if (type.GetType() == Constants.TYPE_RESISTANCE)
            {
                Resistance resistance = new Resistance();
                // Ввод данных в консоле
                resistance.Input();
                // Расчет по формуле
                resistance.Calculate();
                // Вывод данных в консоль
                resistance.Output();
            }

            Console.ReadKey();
        }
    }
}
