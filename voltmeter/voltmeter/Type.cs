﻿using System;
using System.Collections.Generic;
using System.Text;

namespace voltmeter
{
    class Type 
    {
        private int type;

        public void Input()
        {
            Console.WriteLine($"1 - {Constants.STR_VOLTAGE} {Constants.STR_UNIT_VOLTAGE}");
            Console.WriteLine($"2 - {Constants.STR_AMPERAGE} {Constants.STR_UNIT_AMPERAGE}");
            Console.WriteLine($"3 - {Constants.STR_RESISTANCE} {Constants.STR_UNIT_RESISTANCE}");
            Console.Write($"\n{Constants.STR_INPUT_TYPE}");

            type = Convert.ToInt32(Console.ReadLine());
        }

        public new int GetType()
        {
            return type;
        }
    }
}
